class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
        return this._name;
    }

    set name(newName){
        newName = newName.trim();

        if(newName === ''){
            throw "incorrect value";
        }

        this._name = newName;
    }

    get age(){
        return this._age;
    }

    set age(newAge){
        if(Number.isInteger(newAge) && newAge > 0){
            this._age = newAge;
        } else {
            throw "incorrect value";
        }
    }

    get salary(){
        return this._salary;
    }

    set salary(newSalary){
        if(Number.isInteger(newSalary) && newSalary > 0){
            this._salary = newSalary;
        } else {
            throw "incorrect value";
        }
    }
}

let emp1 = new Employee("Jack", 1, 400);

console.log(emp1.name);
console.log(emp1.age);
console.log(emp1.salary);

emp1.name = "Axel";
emp1.age = 5;
emp1.salary = 3;

console.log(emp1.name);
console.log(emp1.age);
console.log(emp1.salary);

class Programmer extends Employee{
    constructor(name, age, salary, lang ){
        super(name, age, salary);
        this._lang = lang;
       
    }

    get lang(){
        return this._lang;
    }

    get salary(){
        return this._salary * 3;    
    }
}

let prog1 = new Programmer("Ken", 24, 2000, ["Java", "Python"]);

console.log(prog1.salary);
console.log(prog1);

let prog2 = new Programmer("Mark", 43, 15000, ["JavaScript"]);

console.log(prog2);

let prog3 = new Programmer("Joe", 20, 800, ["HTML"]);

console.log(prog3);